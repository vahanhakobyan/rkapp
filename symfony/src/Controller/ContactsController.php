<?php

namespace App\Controller;

use App\Entity\Contacts;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactsController extends AbstractController
{
    /**
     * @Route("/contacts", methods={"GET"}, name="contacts")
     */
    public function index(): Response
    {
        $contacts = $this->getDoctrine()->getRepository(Contacts::class);
        return $this->json($contacts->getAll());
    }

    /**
     * @Route("/contact/{id}", methods={"GET"}, name="show_contact")
     */
    public function show($id): Response
    {
        $contact = $this->getDoctrine()->getRepository(Contacts::class)->find($id);

        return $this->json([
            'id' => $contact->getId(),
            'name' => $contact->getName(),
            'email' => $contact->getEmail(),
            'phone' => $contact->getPhone(),
        ]);
    }

    /**
     * @Route("/contact", methods={"POST"}, name="create_contact")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $contact = new Contacts();
        $contact->setName($data['name']);
        $contact->setEmail($data['email']);
        $contact->setPhone($data['phone']);

        $entityManager->persist($contact);
        $entityManager->flush();

        return $this->json([
            'message' => 'ok',
            'insert_id' => $contact->getId()
        ]);
    }

    /**
     * @Route("/contact/{id}", methods={"PUT"}, name="update_contact")
     */
    public function update(int $id, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $contact = $this->getDoctrine()->getRepository(Contacts::class)->find($id);

        $contact->setName($request->get('name'));
        $contact->setEmail($request->get('email'));
        $contact->setPhone($request->get('phone'));

        $entityManager->persist($contact);
        $entityManager->flush();

        return $this->json([
            'message' => 'ok',
            'contact_id' => $id
        ]);
    }

    /**
     * @Route("/contact/{id}", methods={"DELETE"}, name="delete_contact")
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $contact = $this->getDoctrine()->getRepository(Contacts::class)->find($id);

        $entityManager->remove($contact);
        $entityManager->flush();

        return $this->json([
            'message' => 'ok',
            'deleted_id' => $id
        ]);
    }
}
